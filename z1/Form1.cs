﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            labelRez.Text = labelRez.Text + "1";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            labelRez.Text = labelRez.Text + "2";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            labelRez.Text = labelRez.Text + "3";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            labelRez.Text = labelRez.Text + "4";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            labelRez.Text = labelRez.Text + "5";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            labelRez.Text = labelRez.Text + "6";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            labelRez.Text = labelRez.Text + "7";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            labelRez.Text = labelRez.Text + "8";
        }

        private void button9_Click(object sender, EventArgs e)
        {
            labelRez.Text = labelRez.Text + "9";
        }

        private void button0_Click(object sender, EventArgs e)
        {
            if (labelRez.Text != "")
                labelRez.Text = labelRez.Text + "0";
        }

        private void buttonDel_Click(object sender, EventArgs e)
        {
            labelDisplay.Text = "";
            labelRez.Text = "";
        }

        double x;
        private void buttonPlus_Click(object sender, EventArgs e)
        {
            double.TryParse(labelRez.Text, out x);
            labelRez.Text = "";
            labelDisplay.Text = x.ToString() + " + ";
        }

        private void buttonMinus_Click(object sender, EventArgs e)
        {
            double.TryParse(labelRez.Text, out x);
            labelRez.Text = "";
            labelDisplay.Text = x.ToString() + " - ";
        }

        private void buttonPuta_Click(object sender, EventArgs e)
        {
            double.TryParse(labelRez.Text, out x);
            labelRez.Text = "";
            labelDisplay.Text = x.ToString() + " * ";
        }

        private void buttonPod_Click(object sender, EventArgs e)
        {
            double.TryParse(labelRez.Text, out x);
            labelRez.Text = "";
            labelDisplay.Text = x.ToString() + " / ";
        }

        private void buttonJednako_Click(object sender, EventArgs e)
        {
            double y;
            double.TryParse(labelRez.Text, out y);
            labelDisplay.Text = labelDisplay.Text + y.ToString() + " =";
            if (labelDisplay.Text.Contains("+"))
                labelRez.Text = (x + y).ToString();
            else if (labelDisplay.Text.Contains("-"))
                labelRez.Text = (x - y).ToString();
            else if (labelDisplay.Text.Contains("*"))
                labelRez.Text = (x * y).ToString();
            else if (labelDisplay.Text.Contains("/"))
                if (y == 0)
                    MessageBox.Show("Dijelnje s nulom nije dozvoljeno!", "Upozorenje!");
                else
                    labelRez.Text = (x / y).ToString();
        }

        private void buttonKvadrat_Click(object sender, EventArgs e)
        {
            double.TryParse(labelRez.Text, out x);
            labelDisplay.Text = x.ToString() + "^2 =";
            labelRez.Text = (x * x).ToString();
        }

        private void buttonKub_Click(object sender, EventArgs e)
        {
            double.TryParse(labelRez.Text, out x);
            labelDisplay.Text = x.ToString() + "^3 =";
            labelRez.Text = (x * x * x).ToString();
        }

        private void buttonKorijen_Click(object sender, EventArgs e)
        {
            double.TryParse(labelRez.Text, out x);
            labelDisplay.Text = "sqrt(" + x.ToString() + ") =";
            labelRez.Text = Math.Sqrt(x).ToString();
        }

        private void buttonSin_Click(object sender, EventArgs e)
        {
            double.TryParse(labelRez.Text, out x);
            labelDisplay.Text = "sin(" + x.ToString() + ") =";
            x = x * Math.PI / 180;
            labelRez.Text = Math.Sin(x).ToString();
        }

        private void buttonCos_Click(object sender, EventArgs e)
        {
            double.TryParse(labelRez.Text, out x);
            labelDisplay.Text = "cos(" + x.ToString() + ") =";
            x = x * Math.PI / 180;
            labelRez.Text = Math.Cos(x).ToString();
        }

        private void buttonTan_Click(object sender, EventArgs e)
        {
            double.TryParse(labelRez.Text, out x);
            labelDisplay.Text = "tan(" + x.ToString() + ") =";
            x = x * Math.PI / 180;
            labelRez.Text = Math.Tan(x).ToString();
        }

        private void buttonLog_Click(object sender, EventArgs e)
        {
            double.TryParse(labelRez.Text, out x);
            labelDisplay.Text = "log(" + x.ToString() + ") =";
            labelRez.Text = Math.Log10(x).ToString();
        }
    }
}
